# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from rest_framework.response import Response
from rest_framework.decorators import api_view
from twilio.twiml.messaging_response import MessagingResponse

from utils import *

import requests
import json

cache = {
    "stateHash": '0'
}


@api_view(['POST'])
def hello_world(request):
    try:
        print('request.POST[: ', request.POST)
        send_twilio_message_res = twilio_send_message(request.POST['message'], request.POST['phone'])
        return Response(data={'success': True, 'whatsapp_res': send_twilio_message_res.sid}, status=200)
    except Exception as e:
        return Response(data={'success': False}, status=400)


@api_view(['POST'])
def start_covid_flow(request):
    try:
        message = "Hi, we realise that Covid has left us all in unchartered areas. \n Urban Company (formerly UrbanClap) has partnered with Srinidhi Foundation, a charitable trust, to set up a relief fund to support gig workers and independent contractors on its platform during the COVID-19 crisis. \n Read more here - https://economictimes.indiatimes.com/small-biz/startups/newsbuzz/urban-company-to-set-up-relief-fund-for-temporary-workers-contractors/articleshow/74809766.cms?utm_source=contentofinterest&utm_medium=text&utm_campaign=cppst \n If you have queries, please put it in this chat box and we will get back to you!"
        send_twilio_message_res = twilio_send_message(message, request.POST['phone'])
        cache['stateHash'] = 'covid:1'
        return Response(data={'success': True}, status=200)
    except Exception as e:
        return Response(data={'success': False}, status=400)


# Available fields in Twilio Response
# accountSid = request.POST['AccountSid']
# smsMessageSid = request.POST['SmsMessageSid']
# numMedia = request.POST['NumMedia']
# smsSid = request.POST['SmsSid']
# smsStatus = request.POST['SmsStatus']
# body = request.POST['Body']
# to = request.POST['To']
# numSegments = request.POST['NumSegments']
# messageSid = request.POST['MessageSid']
# fromSender = request.POST['From']
# apiVersion = request.POST['ApiVersion']

@api_view(['GET', 'POST'])
def twilio_user_response(request):
    try:
        pyDict = request.POST.dict()
        whatsapp_number = pyDict['From']
        twilio_whatsapp_res = {
            'fromUser': (whatsapp_number),
            'messageBody': request.POST['Body']
        }
        print ("==========================")
        print('state: ', cache)
        print('stateHash: ', cache['stateHash'])
        print('response: ', request.POST['Body'])
        print ("==========================")
        #TODO : If stateHash is 0 and free text message appears
        stateHash = interact(twilio_whatsapp_res, cache['stateHash'])
        return Response(data={'success': True}, status=200)
    except Exception as e:
        print(e)
        return Response(data={'success': False}, status=400)


def interact(twilio_whatsapp_res, stateHash):
    processChoice(twilio_whatsapp_res, stateHash)
    return stateHash


def processChoice(twilio_whatsapp_res, stateHash):
    # FIXME after ucHack : Hard coded proId to get dev data
    provider_id = '5d1c4ff1ced1572400139c45'

    if cache['stateHash'].startswith('covid:1'):
        print twilio_whatsapp_res
        if twilio_whatsapp_res['messageBody'] == "hi" or twilio_whatsapp_res['messageBody'] == "Hi":
            cache['stateHash'] = '0'
            stateHash = '0'
        else:
            send_twilio_message_res = twilio_send_message(
                "We have noted your query and will get back. Please Hi to start UrbanChat again!",
                twilio_whatsapp_res['fromUser'])
            cache['stateHash'] = '0'
            return

    if stateHash == '0':
        introBot(twilio_whatsapp_res)

    elif stateHash[0] == '1':
        userInput = twilio_whatsapp_res['messageBody']
        cache['stateHash'] += userInput
        print('STATE: ', cache['stateHash'])
        state = cache['stateHash'][1:]
        try:
            if (state == '1'):
                handlePayout(twilio_whatsapp_res, provider_id)
            elif (state == '2'):
                lastJobInvoice(twilio_whatsapp_res)
            elif (state == '3'):
                handleLoans(twilio_whatsapp_res, provider_id)
            elif (state == '11'):
                handlePayoutCorrectBankDetails(twilio_whatsapp_res, provider_id)
            elif (state == ('12')):
                handlePayoutIncorrectBankDetails(twilio_whatsapp_res, provider_id)
            else:
                handleDefault(twilio_whatsapp_res)
        except Exception as e:
            handleDefault(twilio_whatsapp_res)
    elif stateHash.startswith('incorrect'):
        twilio_send_message(
            "Thanks account has been updated and you will get 💵 as soon as Monet processes it. Please say Hi to start chat again!",
            twilio_whatsapp_res['fromUser'])
        cache['stateHash'] = '0'
    else:
        cache['stateHash'] = '0'
        message = 'I am little dumb rn. Please say Hi to get UrbanChat started. 🤡'
        send_twilio_message_res = twilio_send_message(message, twilio_whatsapp_res['fromUser'])


def introBot(twilio_whatsapp_res):
    if (twilio_whatsapp_res['messageBody'] == 'hi' or twilio_whatsapp_res['messageBody'] == 'Hi'):
        message = 'Hello! I am UrbanChat.\nHow may I help you today?\n1️⃣ My Payout was not processed\n2️⃣ Last job invoice\n3️⃣ Loans Status\n'
        cache['stateHash'] = '1'
    else:
        message = 'I am little dumb rn. Please say Hi to get UrbanChat started. 🤡'
        cache['stateHash'] = '0'

    send_twilio_message_res = twilio_send_message(message, twilio_whatsapp_res['fromUser'])


def handlePayout(twilio_whatsapp_res, provider_id):
    # FIXME after ucHack : Hard coded proId to get dev data
    provider_id = '5dbac113da55f527005b7ea2'

    try:
        url = "http://service-development-270222222.ap-southeast-1.elb.amazonaws.com:8086/settlement-service/getBankDetail?client_id=service-market"
        headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
        payload = {
            "provider_id": provider_id
        }
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        response = response.json()
        print('response: ', response)
        bank_details = response['bank_details']
        account_number = bank_details['bank_account_number']
        bank_ifsc_code = bank_details['bank_ifsc_code']

        message = """Please check your Bank Details given below.\nAccount Number: %s\nIFSC: %s\n1️⃣They are Correct.\n2️⃣They do not match.""" % (
            account_number, bank_ifsc_code)
        send_twilio_message_res = twilio_send_message(message, twilio_whatsapp_res['fromUser'])
    except Exception as e:
        cache['stateHash'] = '0'


def handlePayoutCorrectBankDetails(twilio_whatsapp_res, provider_id):
    message = 'Payout is delayed today.\nPlease wait for 24 hrs.⏱\nClosing UrbanChat.🙅🏼‍♂️ \nSay hi to Chat again.👋🏼'
    try:
        cache['stateHash'] = '0'
        send_twilio_message_res = twilio_send_message(message, twilio_whatsapp_res['fromUser'])
    except Exception as e:
        cache['stateHash'] = '0'


def handlePayoutIncorrectBankDetails(twilio_whatsapp_res, provider_id):
    # TODO : Update bank details here
    message = 'Please enter your a/c number'
    try:
        cache['stateHash'] = 'incorrect:'
        send_twilio_message_res = twilio_send_message(message, twilio_whatsapp_res['fromUser'])
    except Exception as e:
        cache['stateHash'] = '0'


def handleLoans(twilio_whatsapp_res, provider_id):
    try:
        url = "http://uc-service-development-960667868.ap-southeast-1.elb.amazonaws.com:9026/loan-service/getLoanTrackingDetails?client_id=service-market"
        headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
        payload = {
            "user_id": provider_id
        }
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        response = response.json()

        loanState = (response['details'][0]['status'])

        message = get_loan_state_message(loanState)
        send_twilio_message_res = twilio_send_message(message, twilio_whatsapp_res['fromUser'])
        cache['stateHash'] = '0'


    except Exception as e:
        message = 'Loan not found, Exiting UrbanChat.'
        send_twilio_message_res = (message, twilio_whatsapp_res['fromUser'])
        cache['stateHash'] = '0'


def get_loan_state_message(loanState):
    if (loanState == 'late'):
        return "Your Loan Payment is running late. We will update you💰 \nClosing UrbanChat.🙅🏼‍♂️ \nSay hi to Chat again.👋🏼"
    elif (loanState == 'complete'):
        return "Your Loan Payment is complete. Mubarak ho.💰\nClosing UrbanChat.🙅🏼‍♂️ \nSay hi to Chat again.👋🏼"
    elif (loanState == 'normal'):
        return "Your Loan Payment status is on track.💰\nClosing UrbanChat.🙅🏼‍♂️ \nSay hi to Chat again.👋🏼"
    elif (loanState == 'behind'):
        return "Your Loan Payment status is behind, please leave relevant documents in this chat if payments are already made.💰\nClosing UrbanChat.🙅🏼‍♂️ \nSay hi to Chat again.👋🏼"
    return "Loan State not found 🤔"


def handleDefault(twilio_whatsapp_res):
    message = 'Please sahi choice daaliye warna main confused hoon jata hoon 👊'
    print('message: ', message)
    cache['stateHash'] = '0'
    print('hello: ', twilio_whatsapp_res['fromUser'])
    send_twilio_message_res = twilio_send_message(message, twilio_whatsapp_res['fromUser'])


def lastJobInvoice(twilio_whatsapp_res):
    twilio_send_message_w_media("", twilio_whatsapp_res['fromUser'])
    twilio_send_message("Hope this helps. Please say Hi to start Urban Chat again!", twilio_whatsapp_res['fromUser'])
    cache['stateHash'] = '0'
