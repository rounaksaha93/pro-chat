from twilio.rest import Client
from constants import *

import requests
import json


def twilio_send_message(message, phoneNo):
    try:
        client = Client(TWILIO_SID, TWILIO_AUTH_KEY)
        message = client.messages.create(
            from_='whatsapp:+14155238886',
            body=message,
            to=phoneNo
        )
        return message
    except Exception as e:
        print(e)
        return e


def twilio_send_message_w_media(message, phoneNo):
    try:
        client = Client(TWILIO_SID, TWILIO_AUTH_KEY)
        message = client.messages.create(
            media_url=['https://uchack.s3.us-east-2.amazonaws.com/5e3bfb3f5315372a0032e432.pdf'],
            from_='whatsapp:+14155238886',
            body=message,
            to=phoneNo
        )
        return message
    except Exception as e:
        print(e)
        return e


def get_provider_id(phone_number):
    # Can be Better
    phone_number = phone_number[12:]
    phone_number = "+91 " + phone_number
    print('Phone Number: ', phone_number)

    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    url = "http://service-development-270222222.ap-southeast-1.elb.amazonaws.com:9011/core-service/bulkGetProviderDetails?client_id=service-market"

    arr = [phone_number]
    print(arr[0])
    payload = {
        "phone_numbers": arr,
        "method_name": "fetchCreateRequestDbData_promise"
    }

    response = requests.post(url, data=json.dumps(payload), headers=headers)
    response = response.json()
    provider = response[0]['provider_id']

    return phone_number


if __name__ == '__main__':
    try:
        test_response = {
            'fromUser': 'whatsapp:+919910178440'
        }
        provider_id = get_provider_id('whatsapp:+919910178440')
        print('provider_id: ', provider_id)
    except Exception as e:
        print(e)
